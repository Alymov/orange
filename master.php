<?php
header('Content-Type: text/html; charset=UTF-8');
session_start();
if (!empty($_SESSION['login'])) {
    header('Location: slave.php');
}
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	
    if (!empty($_COOKIE['login_error'])) {
        echo '<div class = "error"> Неверный логин или пароль!</div>';
        setcookie('login_error', '', 1000);
    }
?>

<!DOCTYPE html>
<html lang="ru">
   <head>
      <style>
        .error{
			border: 2px solid red;
			background-color: red;
		}
      </style>
      <meta charset="utf-8">
      <title>LOG IN</title>
   </head>
   <body>
      <div>
         <h2>Log in</h2>
         <form action="" method="POST">
            <label> Логин: <br>
            <input name="login" />
            </label>
            <br>
			
            <label> Пароль:  <br>
            <input name="pass_hash" />
            </label>
			
            <br>
            <br>
            <input type="submit"value="enter" />
            <br>
         </form>
      </div>
   </body>
</html>

<?php
}
else {
    
    $user = 'u17815';
		$pass = '9028103';
		$db = new PDO('mysql:host=localhost;dbname=u17815', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    try {
        $stmt = $db->prepare("SELECT * FROM userData WHERE login = ? AND pass_hash = ?;");
        $stmt->bindValue(1, $_POST['login'], PDO::PARAM_STR);
        $stmt->bindValue(2, md5($_POST['pass_hash']), PDO::PARAM_STR);
        $stmt->execute();
    }
    catch(PDOException $e) {
        print ('Ошибка : ' . $e->getMessage());
        exit();
    }
    
    $user_id = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    if (!empty($user_id)) {
        $_SESSION['login'] = $_POST['login'];
        $_SESSION['pass_hash'] = $_POST['pass_hash'];
        $_SESSION['id'] = $user_id[0]['id'];
        header('Location: slave.php');
    } else {
        setcookie('login_error', 1, 0);
        header('Location: master.php');
    }
}
