<?php
	$user = 'u17815';
	$pass = '9028103';
	$db = new PDO('mysql:host=localhost;dbname=u17815', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
	try {
		$stmt = $db->prepare("SELECT * FROM admins");
		$stmt->execute();
	}
	catch(PDOException $e) {
        print ('Ошибка : ' . $e->getMessage());
        exit();
    }

	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$adminLogin = $rows[0]['login'];
	$passAdmin = $rows[0]['pass'];
	
	if (empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW']) || $_SERVER['PHP_AUTH_USER'] != $adminLogin || md5($_SERVER['PHP_AUTH_PW']) != $passAdmin) {
		header('HTTP/1.1 401 Unanthorized');
		header('WWW-Authenticate: Basic realm="My site"');
		print ('<h1>401 Требуется авторизация.</h1>');
		exit();
	}


	if ($_SERVER['REQUEST_METHOD'] == 'GET') {
		$stmt = $db->prepare("SELECT ID, name, email, login, pass_hash, date, sex, con, bio FROM userData");
		$stmt->execute();
		$users = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$values = [];
		foreach ($users as $user) {
			$ID = !empty($user['ID']) ? $user['ID'] : '';
			$name = !empty($user['name']  || preg_match('/^[а-яА-ЯёЁa-zA-Z ]+$/u', $user['name'])) ? strip_tags($user['name']) : '';
			$email = !empty($user['email']) ? strip_tags($user['email']) : '';
			$date = (!empty($user['date']) || preg_match('/[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])/u', $user['date'])) ? $user['date'] : '';
			$sex = !empty($user['sex']) ? $user['sex'] : '';
			$con = !empty($user['con']) ? $user['con'] : '';
			$bio = !empty($user['bio']) ? strip_tags($user['bio']) : '';
			$values[$user['ID']] = [
				$ID, 
				$name, 
				$email, 
				$date, 
				$sex, 
				$con, 
				$bio, 
			];
		}
		$ammUser = count($values);
		include ('king.php');
	} 
	else {
		$idRemove = [$_POST['idRemove']];
		$user = 'u17815';
		$pass = '9028103';
		$db = new PDO('mysql:host=localhost;dbname=u17815', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
		try {
			$stmt = $db->prepare("SELECT * FROM userData WHERE id = ?;");
			$stmt->execute($idRemove);
		}
		catch(PDOException $e) {
			print ('Ошибка : ' . $e->getMessage());
			exit();
		}
		
		$userRemove = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if (!empty($userRemove)) {
			$stmt = $db->prepare('DELETE FROM userData WHERE id = ?');
			$stmt->execute($idRemove);
			setcookie('save', '1');
			header('Location: god.php');
		} 
		else {
			setcookie('idError', 1, 0);
			header('Location: god.php');
		}
	}
?>
