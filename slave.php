<?php

$token = md5(uniqid(mt_rand() . microtime()));
$_SESSION['token'] = $token;
header('Content-Type: text/html; charset=UTF-8');
$message = '';
$message_name = '';
$message_email = '';
$message_date = '';
$message_con = '';
$message_sex = '';
$message_bio = '';
$message_check = '';
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    
    $messages = array();
    if (!empty($_COOKIE['save'])) {
        setcookie('save', '', 100000);
		setcookie('login', '', 100000);
		setcookie('pass_hash', '', 100000);
        $message = 'УСПЕШНО';
		if (!empty($_COOKIE['pass_hash'])) {
		$message = sprintf('<a href="master.php">Войдите</a> с логином <strong>%s</strong> и паролем <strong>%s</strong> для внесения изменений.', strip_tags($_COOKIE['login']), strip_tags($_COOKIE['pass_hash']));
		}
    }

$errors = array();
  $errors['name'] = !empty($_COOKIE['name_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['date'] = !empty($_COOKIE['date_error']);
  $errors['con'] = !empty($_COOKIE['con_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['bio'] = !empty($_COOKIE['bio_error']);
  $errors['check'] = !empty($_COOKIE['check_error']);



if ($errors['name']) {
    setcookie('name_error', '', 100000);
    if($_COOKIE['name_error'] == '1') 
	{
      $message_name = '<div class="error"> Пустое поле у имени</div>';
    } 
}

if ($errors['email']) {
    setcookie('email_error', '', 100000);
    if($_COOKIE['email_error'] == '1') 
	{
      $message_email = '<div class="error"> Пустое поле в почте</div>';
    } 
	else 
	{
      $message_email = '<div class="error"> Неверные символы в почте</div>';
    }
  }


if ($errors['date']) {
    setcookie('date_error', '', 100000);
    $message_date = '<div class="error"> Неверная дата </div>';
  }


if ($errors['sex']) {
    setcookie('sex_error', '', 100000);
    $message_sex = '<div class="error"> Не выбран пол </div>';
  }

if ($errors['con']) {
    setcookie('con_error', '', 100000);
    $message_con = '<div class="error"> Не выбраны конечности </div>';
}

if ($errors['bio']) {
    setcookie('bio_error', '', 100000);
    $message_bio = '<div class="error"> Пустая биография </div>';
}

if ($errors['check']) {
    setcookie('check_error', '', 100000);
    $message_check = '<div class="error"> СОГЛАСИТЕСЬ!!! </div>';
}

$values = array();
$values['name'] = (empty($_COOKIE['name_value']) || !preg_match('/^[а-яА-ЯёЁa-zA-Z ]+$/u', $_COOKIE['name_value'])) ? '' : strip_tags($_COOKIE['name _value']);
$values['email'] = (empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']));
$values['date'] = (empty($_COOKIE['date_value']) || !preg_match('/[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])/u', $_COOKIE['date_value'])) ? '' : $_COOKIE['date_value'];
$values['sex'] = (empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value']);
$values['con'] = (empty($_COOKIE['con_value']) ? '' : $_COOKIE['con_value']);
$values['bio'] = (empty($_COOKIE['bio_value']) ? '' : strip_tags($_COOKIE['bio_value']));
$values['check'] = (empty($_COOKIE['check_value']) ? ' ' : $_COOKIE['check_value']);

if (!in_array(1, $errors) && !empty($_COOKIE[session_name() ]) && session_start() && !empty($_SESSION['login'])){
		$pass_hash = md5($_SESSION['pass_hash']);
		$user = 'u17815';
		$pass = '9028103';
		$db = new PDO('mysql:host=localhost;dbname=u17815', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
		try {
				$stmt = $db->prepare('SELECT name, email, date, sex, con, bio FROM userData WHERE login = ? AND pass_hash = ?;');
				$stmt->bindValue(1, $_SESSION['login'], PDO::PARAM_STR);
				$stmt->bindValue(2, $pass_hash, PDO::PARAM_STR);
				$stmt->execute();
			}
			catch(PDOException $e) {
				print ('Ошибка : ' . $e->getMessage());
				exit();
			}
			
			$userData = $stmt->fetchAll(PDO::FETCH_ASSOC) [0];
			$values['name'] = !empty($userData['name']) ? $userData['name'] : '';
			$values['email'] = !empty($userData['email']) ? $userData['email'] : '';
			$values['date'] = !empty($userData['date']) ? $userData['date'] : '';
			$values['sex'] = !empty($userData['sex']) ? $userData['sex'] : '';
			$values['con'] = !empty($userData['con']) ? $userData['con'] : '';
			$values['bio'] = !empty($userData['bio']) ? $userData['bio'] : '';
			printf('Добро пожаловать, %s. Ваш идентификационный номер: %d.', $_SESSION['login'], $_SESSION['id']);
			
			
	}

include('work.php');
}

else {
    $errors = FALSE;
	
    if (!(preg_match('/^[а-яА-ЯёЁa-zA-Z ]+$/u', $_POST['name']))) {
        setcookie('name_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('name_value', $_POST['name'], time() + 365 * 24 * 60 * 60);
    }


		
	if (!(preg_match('/^([A-Za-z0-9_\-\.])+@([A-Za-z0-9_\-\.])+\.([A-Za-z0-9])+$/', $_POST['email'])) ) {
		setcookie('email_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
	}

	if (!(preg_match('/^\d\d\d\d\-\d\d\-\d\d$/', $_POST['date']))) {
		setcookie('date_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('date_value', $_POST['date'], time() + 365 * 24 * 60 * 60);
	}

	if ( empty($_POST['sex'])) {
		setcookie('sex_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('sex_value', $_POST['sex'], time() + 365 * 24 * 60 * 60);
	}
	

	if (empty($_POST['con'])) {
		setcookie('con_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('con_value', $_POST['con'], time() + 365 * 24 * 60 * 60);
	}

	if (empty($_POST['bio'])) {
		setcookie('bio_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		setcookie('bio_value', $_POST['bio'], time() + 365 * 24 * 60 * 60);
	}

	if (empty($_POST['check'])) {
		setcookie('check_error', '1', time() + 24 * 60 * 60);
		$errors = TRUE;
	}
	else {
		$check = $_POST['check'] ? '1' : '0';
		setcookie('check_value', $check, time() + 365 * 24 * 60 * 60);
	}
	$check = $_POST['check'] ? '1' : '0';

    if ($errors) {
        header('Location: slave.php');
        exit();
    }
    else {
        setcookie('name_error', '', 100000);
        setcookie('mail_error', '', 100000);
        setcookie('data_error', '', 100000);
        setcookie('sex_error', '', 100000);
        setcookie('con_error', '', 100000);
        setcookie('bio_error', '', 100000);
        setcookie('check_error', '', 100000);
    }
setcookie('save', '1');

if (!empty($_COOKIE[session_name() ]) && session_start() && !empty($_SESSION['login'])) {
        
        $user = 'u17815';
		$pass = '9028103';
		$db = new PDO('mysql:host=localhost;dbname=u17815', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
		
        try {
            $stmt = $db->prepare("UPDATE userData SET name = ?, email = ?, date = ?, sex = ?, con = ?, bio = ?, WHERE id = ?;");
            $stmt->execute(array(mysqli_real_escape_string($db, $_POST['name']), mysqli_real_escape_string($db, $_POST['email']), mysqli_real_escape_string($db, $_POST['date']), mysqli_real_escape_string($db, $_POST['sex']), mysqli_real_escape_string($db, $_POST['con']), mysqli_real_escape_string($db, $_POST['bio']), 1, $_SESSION['id']));
        }
        catch(PDOException $e) {
            print ('Ошибка : ' . $e->getMessage());
            exit();
        }
    } else {
        $login = $_POST['email'];
        $login.= "_login";
        $pass_hash = substr(uniqid(), 0, 8);
        setcookie('login', $login);
        setcookie('pass_hash', $pass_hash);
        
        $user = 'u17815';
		$pass = '9028103';
        $db = new PDO('mysql:host=localhost;dbname=u17815', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
        try {
            $stmt = $db->prepare("INSERT INTO userData (name, email, login, pass_hash, date, sex, con, bio) VALUES (:name, :email, :login, :pass_hash, :date, :sex, :con, :bio);");
			$name = mysqli_real_escape_string($db, $_POST['name']);
            $stmt->bindParam(':name', $name);
			$email = mysqli_real_escape_string($db, $_POST['email']);
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':login', $login);
			$pass_hash = mysqli_real_escape_string($db, md5($pass_hash));
            $stmt->bindParam(':pass_hash', $pass_hash);
			$date = mysqli_real_escape_string($db, $_POST['date']);
            $stmt->bindParam(':date', $date);
			$sex = mysqli_real_escape_string($db, $_POST['sex']);
            $stmt->bindParam(':sex', $sex);
			$con = mysqli_real_escape_string($db, $_POST['con']);
            $stmt->bindParam(':con', $con);
			$bio = mysqli_real_escape_string($db, $_POST['bio']);
            $stmt->bindParam(':bio', $bio);
            $stmt->execute();
        }
        catch(PDOException $e) {
            print ('Ошибка : ' . $e->getMessage());
            exit();
        }
    }  
setcookie('save', '1');
header('Location: slave.php');
}
?>
